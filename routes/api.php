<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth:api');

Route::group(['prefix'=>'/timeline'],function(){
    Route::group(['prefix'=>'/post'],function(){
        $con = 'TimelineController';
        Route::get('/',$con.'@fetchPosts');
        Route::post('/',$con.'@newPost');
        Route::put('/',$con.'@editPost');
        Route::delete('/',$con.'@deletePost');
        Route::post('/like',$con.'@newLike');
});
    Route::group(['prefix'=>'/user'],function(){

    });
});

Route::group(['prefix'=>'/account'],function(){
    Route::group(['prefix'=>'/user'],function(){
        $con = 'AccountController';
        Route::post('/login',$con.'@login');
        Route::post('/new',$con.'@register');
    });
});