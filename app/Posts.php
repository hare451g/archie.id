<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Posts extends Model
{
    const CREATED_AT = 'cretime';
    const UPDATED_AT = 'modtime';
    
    protected $table = 'posts';
    
    protected $guarded = ['id'];
    protected $primaryKey = 'id';
      
    public function Users(){
        return $this->belongsTo('App\Users','user_id');
    }
    
    public function Likes(){
        return $this->hasMany('App\Likes','post_id');
    }
    
    public function Comments(){
        return $this->hasMany('App\Comments','post_id');
    }
}
