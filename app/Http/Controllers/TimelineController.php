<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Posts;
use App\Likes;

class TimelineController extends Controller
{    
    /* New Posts
    /   @params : 
            Header 
                {
                    'token' (varchar)
                }
            Request ( application/json )
               {
                'content' (varchar)
               }
    /   @return : 
               {
                   'success' 1 / 0,
                   'message' (text)
               }
    */
    public function newPost(Request $req){
        // initialize default response
        $helper = new HelperController;
        $res = $helper->defaultResponse();
        // simplify values
        $content = $req->content;
        $token = $req->header('Token');
        // check user exists
        $userInfo = $helper->checkToken($token);
        if($userInfo['exists']){
            $userID = $userInfo['id'];
            $post = new Posts;
            // Setup data
            $data = array(
                'user_id' => $userID,
                'content' => $content
            );
            // insert
            $post->insert($data);
            $res['success'] = 1;
            $res['message'] = "Posting succeed";
        }else{
            $res['message'] = "Error Posting, please login";
        }
        return response($res)->header('content-type', 'application/json');
    }
     /* Edit selected post
    /   @params : 
            Header 
                {
                    'token' (varchar)
                }
            Request ( application/json )
               {
                'post_id' (int)
                'content' (varchar)
               }
    /   @return : 
               {
                   'success' 1 / 0,
                   'message' (text)
               }
    */
    public function editPost(Request $req){
        // initialize default response
        $helper = new HelperController;
        $res = $helper->defaultResponse();
        // simplify values
        $content = $req->content;
        $postID = $req->post_id;
        $token = $req->header('Token');
        // check user exists
        $userInfo = $helper->checkToken($token);
        if($userInfo['exists']){
            $userID = $userInfo['id'];
            $post = new Posts;
            // Setup updated data
            $data = array(
                'content' => $content
            );
            //Setup where condition
            $condition = array(
                'user_id' => $userID,
                'id' => $postID
            );
            // updating data
            $post->where($condition)->update($data);
            $res['success'] = 1;
            $res['message'] = "Editing post #$postID, succeed";
        }else{
            $res['message'] = "Error editing post #$postID, please login";
        }
        return response($res)->header('content-type', 'application/json');
    }
    /* Delete selected post
    /   @params : 
            Header 
                {
                    'token' (varchar)
                }
            Request ( application/json )
               {
                'post_id' (int)
               }
    /   @return : 
               {
                   'success' 1 / 0,
                   'message' (text)
               }
    */
    public function deletePost(Request $req){
        // initialize default response
        $helper = new HelperController;
        $res = $helper->defaultResponse();
        // simplify values
        $content = $req->content;
        $postID = $req->post_id;
        $token = $req->header('Token');
        // check user exists
        $userInfo = $helper->checkToken($token);
        if($userInfo['exists']){
            $userID = $userInfo['id'];
            $post = new Posts;
            //Setup where condition
            $condition = array(
                'id' => $postID,
                'user_id' => $userID
            );
            // updating data
            $post->where($condition)->delete();
            $res['success'] = 1;
            $res['message'] = "Deleting post #$postID, succeed";
        }else{
            $res['message'] = "Error deleting post #$postID, please login";
        }
        return response($res)->header('content-type', 'application/json');
    }
    /* Fetch All posts
    /   @params : 
            Header 
                {
                    'token' (varchar)
                }
    /   @return : 
               {
                   'success' 1 / 0,
                   'message' (text)
               }
    */
    public function fetchPosts(Request $req){
        // initialize default response
        $helper = new HelperController;
        $res = $helper->defaultResponse();
        // simplify values
        $token = $req->header('Token');
        // check user exists
        $userInfo = $helper->checkToken($token);
        if($userInfo['exists']){
            
            $post = new Posts;

            $data = $post->with([ 
                'users' => function($q){
                    $q->select('id','name');
                },
                'likes' => function($q){
                    $q->select('id','post_id','user_id');
                }
            ])->get();

            $res['success'] = 1;
            $res['message'] = "Fetched ".$data->count()." Posts";
            $res['data'] = $data;
        }else{
            $res['message'] = "Error Posting, please login";
        }
        return response($res)->header('content-type', 'application/json');
    }
     /* New Like
    /   @params : 
            Header 
                {
                    'token' (varchar)
                }
            Request ( application/json )
               {
                'post_id' (int)
               }
    /   @return : 
               {
                   'success' 1 / 0,
                   'message' (text)
               }
    */
    public function newLike(Request $req){
        // initialize default response
        $helper = new HelperController;
        $res = $helper->defaultResponse();
        // simplify values
        $postID = $req->post_id;
        $token = $req->header('Token');
        // check user exists
        $userInfo = $helper->checkToken($token);
        if($userInfo['exists']){
            $userID = $userInfo['id'];
            $like = new Likes;
            $check = $helper->checkLike($userID,$postID);

            if($check['exists']){
                $like->where('id', $check['id'])->delete();
                $res['success'] = 1;
                $res['message'] = "Unlike succeed";
            }else{
                // Setup data
                $data = array(
                    'user_id' => $userID,
                    'post_id' => $postID
                );
                // insert
                $like->insert($data);
                $res['success'] = 1;
                $res['message'] = "Like succeed";
            }
        }else{
            $res['message'] = "Like failed";            
        }
        return response($res)->header('content-type', 'application/json');
    }

}
