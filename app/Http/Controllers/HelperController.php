<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Users;
use App\Posts;
use App\Likes;

class HelperController extends Controller
{
    // setup default response
    public function defaultResponse(){
        return array(
            'success' => 0, 
            'message' => "Unknown error !"
        );
    }
    // setup token rejected response
    public function tokenRejectedResponse(){
        return array(
            'success' => 0,
            'message' => "Token rejected"
        );
    }
    // check token 
    public function checkToken($token){
        $users = new Users;
        $check = $users->where('token',$token)->first();
        if($check){
            return array(
                'id' => $check->id,
                'exists' => true
            );
        }else{
            return array(
                'exists' => false
            );
        }
    }
    // check like 
    public function checkLike($userID,$postID){
        $like = new Likes;
        $check = $like->where(['user_id' => $userID, 'post_id' => $postID])
            ->first();

        if($check){
            return array(
                'id' => $check->id,
                'exists' => true
            );
        }else{
            return array(
                'exists' => false
            );
        }
    }
}
