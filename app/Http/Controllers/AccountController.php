<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Users;

class AccountController extends Controller
{
    /* Check name
    /   @params : name (String)
        @return : boolean
    */
    public function checkname($name){
        // initialize users model
        $users = new Users;
        // check user exist
        $exist = $users->where('name', $name)->get();
        if($exist->count() >= 1){
            return true;
        }else{
            return false;
        }
    }
    /* Register user
    /   @params : Request ( application/json )
               {
                'name' (varchar)
                'password' (varchar)
               }
    /   @return : 
            {
                'success' 1 / 0,
                'message' (text),
                'token' (varchar) / NULL
            }
    */
    public function register(Request $req){
        // initialize default response
        $helper = new HelperController;
        $res = $helper->defaultResponse();
        // simplify values
        $name = $req->name;
        $password = $req->password;
        // check if name exists
        if($this->checkname($name)){
            $res['message'] = "User $name is exist";
        }else{
            // initialize users model
            $users = new Users;
            // setup data
            $data = array(
                'name' => $name,
                'password' => Hash::make($password),
                'token' => encrypt($name.'|'.$password)
            );
            // insert data to user model
            try{
                $users->insert($data);
                $res['success'] = 1;
                $res['message'] = "Register succeed !";
            }catch(Exception $e){
                $res['message'] = "Register error $e !";
            }
        }
        return response($res)->header('content-type', 'application/json');
    }

    /* Login user
    /   @params : Request ( application/json )
               {
                'name' (varchar)
                'password' (varchar)
               }
    /   @return : 
               {
                   'success' 1 / 0,
                   'message' (text),
                   'token' (varchar) / NULL
               }
    */
    public function login(Request $req){
        // initialize default response
        $helper = new HelperController;
        $res = $helper->defaultResponse();
        // simplify values
        $name = $req->name;
        $password = $req->password;
        // check user exist\
        if($this->checkname($name) ){
            // initialize users model
            $users = new Users;
            // matching process 
            $check = $users->where('name',$name)
             ->first();
             if(Hash::check($password,$check->password) ){
                // setup response
                $res['success'] = 1;
                $res['message'] = "Login succeed";
                $res['token'] = $check->token;
             }else{
                $res['message'] = "Password did not match";
             }
        }else{
            $res['message'] = "User $name did not exist";
        }
        return response($res)->header('content-type', 'application/json');
    }
}
