<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Likes extends Model
{
    
    const CREATED_AT = 'cretime';
    
    protected $table = 'likes';

    protected $guarded = ['id'];
    protected $primaryKey = 'id';

    public function Users(){
        return $this->belongsTo('App\Users','user_id');
    }

    public function Posts(){
        return $this->belongsTo('App\Posts','post_id');
    }
}
