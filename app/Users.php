<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Users extends Model
{
    const CREATED_AT = 'cretime';

    protected $table = 'users';

    protected $guarded = ['id'];
    protected $primaryKey = 'id';
  
    public function Followers(){
      return $this->hasMany('App\Followers','domain_user_id');
    }

    public function Posts(){
        return $this->hasMany('App\Posts','user_id');
    }

    public function Likes(){
        return $this->hasMany('App\Likes','user_id');
    }

    public function Comments(){
        return $this->hasMany('App\Comments','user_id');
      }
}
